# Français : Ce code a été élaboré par Adrien Pajot le 11 novembre 2020 et a pour but de pouvoir transformer facilement le fichier excel
# produit par la base de données biolovision : faune-france.org pour le transférer vers eBird 

# English : This code has been elaborated by Adrien Pajot on the 11th of november 2020 and aims to easily arrange data 
# producted by biolovision data base : faune-france.org to transfer them to eBird 

##### Script

install.packages("DataCombine")
install.packages("lubridate")
install.packages("readxl")
install.packages("stringr")

library(DataCombine)
library(lubridate)
library(readxl)
library(stringr)

folder = "~/Desktop/eBird-transfert"  #entrer ici le chemin vers le dossier comportant vos fichiers / set here the route to the folder containing your files 
ncol = 44 #entrer ici le nombre de colonnes de votre fichier / enter here the number of columns (default on faune-france is 44)


setwd(folder)

lf = list.files(folder)

for(filename in lf){
  col_types_text = c()
  for(n in 1:ncol){
    print(n)
    col_types_text = append(col_types_text, "text")
  }
  datafile <- read_excel(filename, col_types = col_types_text)
  
  datafile = datafile[-1,]
  
  datafile$ID_FORM = as.numeric(as.character(datafile$ID_FORM))
  datafile$Common_name = as.character(datafile$NAME_SPECIES)
  datafile$Genus = " " 
  datafile$Species = " " 
  datafile$Number = as.numeric(as.character(datafile$TOTAL_COUNT))
  #datafile$Species_Comments = as.character(datafile$COMMENT) #pour l'instant on va juste remplir vide / for now we leave it empty 
  datafile$Species_Comments = " "
  datafile$Location_name = as.character(datafile$PLACE)
  datafile$Latitude = as.numeric(as.character(datafile$COORD_LAT))
  datafile$Longitude = as.numeric(as.character(datafile$COORD_LON))
  datafile$Date = NA
  datafile$Date <- format(as.Date(as.numeric(datafile$DATE), format = "%Y-%m-%d", origin = "1899-12-30"), "%m/%d/%Y")
  datafile$Start_Time = " " 
  datafile$State_Province = " "  #A améliorer avec la liste des départements et leur correspondance avec les numéros 
  datafile$Country_code = "FR"
  datafile$Protocol = " " 
  datafile$Number_of_Observers = " " 
  datafile$Duration = " " 
  datafile$FULL_FORM = as.numeric(as.character(datafile$FULL_FORM))
  datafile$All_obs_reported = ifelse(datafile$FULL_FORM==1, "Y", "N")
  
  final_datafile = datafile[,(ncol+1):(ncol+16)]
  
  write.table(final_datafile, paste("file_ready_to_import_", substr(filename, 1, (str_length(filename)-5)), ".csv", sep=""), col.names = F, row.names = F, sep=",")
}


		
