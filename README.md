# Transférer des données de Biolovision à eBird / Transfer data from Biolovision plaftorms to eBird 

FR : En Europe, les plateformes Biolovision comme faune-france.org sont nombreuses et concentrent un grand nombre de données. Dans le reste du monde, eBird apparait majoritaire. Le but de ce projet est de favoriser le transfer de vos données des bases Biolovision à votre compte eBird. 

EN : In Europe, Biolovision platforms are widely used. In the rest of the world, mostly eBird is. The goal of this project is to favor data transfer from Biolovision platforms (for example faune-france.org) to your eBird account. 

# Commencer / Getting started

### Télécharger R / Donwload R : https://cran.r-project.org/bin/

* Linux : https://cran.r-project.org/bin/linux/
* MAC : https://cran.r-project.org/bin/macosx/ 
* Windows : https://cran.r-project.org/bin/windows/base/ 

### Télécharger RStudio / Download RStudio 

https://www.rstudio.com/products/rstudio/download/#download

### Créer un dossier "Biolovision-eBird" / Create a folder "Biolovision-eBird"

FR: Ce dossier sera votre répertoire de travail, nous allons le compléter 

EN: This folder will be your working directory, we will complete it

D'abord, télécharger le script R et ranger le dans le dossier / First, download the R script and put it inside the folder :
https://gitlab.com/Adrien_Pajot/transfer-data-from-biolovision-plaftorms-to-ebird/-/blob/main/Biolovision_to_eBird.R

![Download_script](/uploads/9e53c8d1db31c634f718a6f92f82226e/Download_script.png)

Ensuite, toujours dans ce dossier, créer 2 dossiers / Then, still inside the main folder create two folfers : 
* to_import 
* imported 

### Commencer le transfert / Start the transfer 

#### Récupérer vos données Biolovision sur une période donnée / Download your Biolovision data on a defined period 

* Aller sur votre portail faune / Go on your Biolovision portail 
* Connectez-vous / Sign up 
* Cliquez sur consulter des données / Clic on the equivalent of "Consult data" 
* Sélectionner la période voulue / Select the period you want : 

![Date_criteria](/uploads/d975007cacdc7231db5a16e38ce0c918/Date_criteria.png)

* Sélectionner SEULEMENT vos données / Select ONLY your data : 

![Your_data](/uploads/bbcd45bac7a016b68d8b39c45db1aee3/Your_data.png)

* Télécharger le fichier XSLX / Download the XLSX file : 

![XLSX_download](/uploads/a63c53630bf5f2937f8ce0991b2f8b81/XLSX_download.png)

![Your_file](/uploads/6239d8e0d99d6c326aaa62a2538cd013/Your_file.png)

* Ranger le dans le dossier "to_import" de votre répertoire de travail / Put it in the "to_import" folder of your working directory

#### Lancer le script R / Launch R script 

* Ouvrir le script Biolovision_to_eBird.R avec RStudio / Open the Biolovision_to_eBird.R on RStudio 

* Changer le chemin de votre dossier "to_import" au début du script (ligne 19) / Change the path to the "to_import" folder at the beginning of the script (line 19): 

``` folder = "~/Desktop/Biolovision_to_eBird/to_import" ```

FR: Par exemple ici, le dossier "to_import" est dans le dossier "Biolovision_to_eBird" situé sur mon bureau. 
EN: For example here, the "to_import" is inside the "Biolovision_to_eBird" folder on my desktop

Pour connaitre le chemin vers votre dossier / To know your folder path : 
_ documentation à venir _ 

* Une fois changé, lancer tout le script d'un coup / Once changed, launch all the script in one time :
MAC : command+a puis/then command+entrer (EN:enter)
Windows : ctrl+a puis/then ctrl+entrer (EN:enter)

* Un fichier a du apparaitre dans votre dossier "to_import" s'appelant "file_ready_to_import_nomdufichier"
* One file should appeared in your "to_import" folder named "file_ready_to_import_nomdufichier"

#### Dernière étape : Importer le fichier sur eBird / Last step : Import the file on eBird 

* Premièrement, vérifier que votre fichier final fait moins d'1 Mo / First, verify that your file is under 1Mo
* Connectez-vous à votre compte eBird sur votre ordinateur / Connect on your eBird account on your computer
* Cliquer sur "Soumettre" / Clic on "Submit"
* Cliquer sur "Importer des données" / Clic on "Import Data"
* Ajouter votre fichier "ready_to_import" / Add your "ready_to_import" file 
* Choisissez le format "Mention eBird (étdendu)" / Choose "eBird Record Format (Extended)" 
* Cliquez sur "Importer votre fichier" / Clic on "Import file" 

#### Et le tour est joué ! / And that's done ! 

FR: Vous n'avez plus qu'à vérifier que votre import s'est bien passé et à faire les correspondances entre les espèces et les lieux, ce qui est du ressort d'eBird ! 
EN: Now it is on eBird and you only have to check the correspondence between species names and place, which is well explained on eBird ! 

## Informations supplémentaires / Supplementary information 

FR: Toutes les captures d'écran concernent la plateforme Biolovision sont issues de faune-france.org

EN: All the screenings for Biolovision platform are from faune-france.org

## Contribuer ou poser des questions / Contributing or ask questions 

FR: Pour contribuer vous pouvez créer une issue (dans l'onglet à gauche) en explqiant votre problème.
Vous pouvez aussi me contacter par email : pajot.adrien@wanadoo.fr 

EN: To contribute, you can create an issue and explain your problem or your suggestions. 
You can also contact me by email : pajot.adrien@wanadoo.fr

## License

FR: Ce projet est sous une license Open Source qui est présentée dans ce répertoire ! 

EN: This project is under an Open Source license which is presented in the repository ! 

## Project status

FR: Le projet a été créé le 18 août. Chaque issue est reçue par email et devrait être résolue rapidement. 

EN: The project has been created the 18th of August 2021. 
Each issue is received by email and should be answered rapidly. 

